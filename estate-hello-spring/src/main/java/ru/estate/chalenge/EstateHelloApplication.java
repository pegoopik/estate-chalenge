package ru.estate.chalenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class EstateHelloApplication {

    public static void main(String[] args) {
        SpringApplication.run(EstateHelloApplication.class, args);
    }
}